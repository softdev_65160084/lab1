package com.mycompany.mavenproject1;

import java.util.Scanner;

public class Lab2 {
    int count = 0;
    static int row;
    static int col;
    static char currentPlayer = 'o';
    static char table[][] = new char[3][3];
    static Scanner kb = new Scanner(System.in);

    private static void createTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }
        }
    }

    private static void displayBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println("\n-------------");
        }
    }

    private static void inputPlayer(char currentPlayer) {
        boolean correct = false;
        while (correct == false) {
            do {
                System.out.printf("Player " + currentPlayer + " : Please enter ur row, col: ");
                row = kb.nextInt() - 1;
                col = kb.nextInt() - 1;
            } while (row < 0 || col < 0 || row > 2 || col > 2);
            if (table[row][col] == '-') {
                table[row][col] = currentPlayer;
                correct = true;
            } else {
                System.out.println("Invalid move. Please try again.");
            }
        }
    }
    // private static void checkMove(char[][] table, char currentPlayer) {
    // boolean correct = false;
    // while (correct == false) {
    // if (table[row][col] == '-') {
    // table[row][col] = currentPlayer;
    // displayBoard();
    // correct = true;
    // } else {
    // System.out.println("Invalid move. Please try again.");
    // }
    // }
    // }

    private static void switchTurn() {
        if (currentPlayer == 'o') {
            currentPlayer = 'x';
        } else {
            currentPlayer = 'o';
        }
    }

    private static boolean checkWin(char[][] table, char currentPlayer) {
        // Win conditions for Player 1 (O)
        if (table[0][0] == currentPlayer && table[0][1] == currentPlayer && table[0][2] == currentPlayer ||
                table[1][0] == currentPlayer && table[1][1] == currentPlayer && table[1][2] == currentPlayer ||
                table[2][0] == currentPlayer && table[2][1] == currentPlayer && table[2][2] == currentPlayer ||
                table[0][0] == currentPlayer && table[1][0] == currentPlayer && table[2][0] == currentPlayer ||
                table[0][1] == currentPlayer && table[1][1] == currentPlayer && table[2][1] == currentPlayer ||
                table[0][2] == currentPlayer && table[1][2] == currentPlayer && table[2][2] == currentPlayer ||
                table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer ||
                table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            System.out.println("Player " + currentPlayer + " wins.");
            return true;
        } else {
            return false;
        }
    }

    private static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false; // If any empty cell is found, the table is not full
                }
            }
        }
        return true; // All cells are occupied, table is full
    }

    public static void main(String[] args) {
        createTable();
        displayBoard();
        while (checkWin(table, currentPlayer) == false && checkDraw() == false) {
            inputPlayer(currentPlayer);
            displayBoard();
            if(checkWin(table, currentPlayer) == true){
                break;
            }
            if(checkDraw() == true){
                break;
            }
            switchTurn();
        }
    }
}