/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.mavenproject1;
import java.util.Scanner;
/**
 *
 * @author Siwak
 */

public class Lab1 {
    Scanner kb = new Scanner(System.in);
    private char[][] table;
    private int row,col;

    public Lab1 (){
        table = new char[3][3];
        createTable();
    }

    private void createTable(){ //create table
            for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                table[i][j]='-';
                }
            }
        }

    private void displayBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println("\n-------------");
        }
    }

    private void inputP1(){
        boolean correct = false;
        while(correct == false){
            System.out.println("Player1, pls enter ur row,col O: ");
            row = kb.nextInt();
            col = kb.nextInt();
            row -= 1;
            col -= 1;
            if (table[row][col] == '-'){
                table[row][col] = 'O';
                displayBoard();
                correct = true;
            }
            else{
                System.out.println("Invalid move. Please try again.");
            }
            System.out.println();
        }
    }
    private void inputP2(){
        boolean correct = false;
        while(correct == false){
            System.out.println("Player2 X, pls enter ur row,col X: ");
            row = kb.nextInt();
            col = kb.nextInt();
            row -= 1;
            col -= 1;
            if (table[row][col] == '-'){
                table[row][col] = 'X';
                displayBoard();
                correct = true;
            }
            else{
                System.out.println("Invalid move. Please try again. ");
            }
            System.out.println();
        }
    }

    private boolean checkTable(){
        if(table[row][col]!='-'){
            System.out.println("Invalid move. Please try again.");
            return false;
        }
        return true;
    }

    

    boolean gameWon = false;
    
    private void playGame() {
        createTable();
        displayBoard();
    
        while (true) {
            inputP1();
            if (checkWin()) {
                break;
            }

            if (checkDraw()) {
                System.out.println("The game is a draw, no one win.");
                break;
            }

            inputP2();
            if (checkWin()) {
                break;
            }

            if (checkDraw()) {
                System.out.println("The game is a draw, no one win.");
                break;
            }
        }
    }

    private boolean checkWin() {
        // Win conditions for Player 1 (O)
        if (table[0][0] == 'O' && table[0][1] == 'O' && table[0][2] == 'O' ||
            table[1][0] == 'O' && table[1][1] == 'O' && table[1][2] == 'O' ||
            table[2][0] == 'O' && table[2][1] == 'O' && table[2][2] == 'O' ||
            table[0][0] == 'O' && table[1][0] == 'O' && table[2][0] == 'O' ||
            table[0][1] == 'O' && table[1][1] == 'O' && table[2][1] == 'O' ||
            table[0][2] == 'O' && table[1][2] == 'O' && table[2][2] == 'O' ||
            table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O' ||
            table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O') {
            System.out.println("Player 1 wins.");
            return true;
        }
    
        // Win conditions for Player 2 (X)
        if (table[0][0] == 'X' && table[0][1] == 'X' && table[0][2] == 'X' ||
            table[1][0] == 'X' && table[1][1] == 'X' && table[1][2] == 'X' ||
            table[2][0] == 'X' && table[2][1] == 'X' && table[2][2] == 'X' ||
            table[0][0] == 'X' && table[1][0] == 'X' && table[2][0] == 'X' ||
            table[0][1] == 'X' && table[1][1] == 'X' && table[2][1] == 'X' ||
            table[0][2] == 'X' && table[1][2] == 'X' && table[2][2] == 'X' ||
            table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X' ||
            table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X') {
            System.out.println("Player 2 wins.");
            return true;
        }
    
        return false;
    }
    private boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false; // If any empty cell is found, the table is not full
                }
            }
        }
        return true; // All cells are occupied, table is full
    }
        public static void main(String[] args){
        Lab1 game = new Lab1();
        game.playGame();

    }
}